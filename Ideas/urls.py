from django.conf.urls import patterns, include, url
from django.shortcuts import HttpResponseRedirect, redirect
from django.views.generic.simple import redirect_to
from django.core.urlresolvers import reverse
from django.contrib   import admin

admin.autodiscover()

urlpatterns = patterns('',
                       #Home page
                       url(r'^$', 'django.views.generic.simple.redirect_to', {'url': '/projects/'}),
                       # The admin site
                       url(r'^admin/', include(admin.site.urls)),
                       url(r'^accounts/login/$', 'django.contrib.auth.views.login', name='auth_login'),
                       url(r'^accounts/logout/$', 'django.contrib.auth.views.logout', name='auth_logout'),
                       # The projects
                       url(r'^projects/', include('projects.urls'))
)
