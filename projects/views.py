from django.views.generic import DetailView, ListView
from django.shortcuts import HttpResponseRedirect, render
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from models import Project
from forms import CreateForm

class ProjectListView(ListView):
	model=Project
	def get_template_names(self):
		return 'project_list.html'

	def get_queryset(self):
		return Project.objects.all().order_by('-mod_date')[:50]

	def get_context_data(self, **kwargs):
		context = super(ProjectListView, self).get_context_data(**kwargs)
		context['context_object_name'] = 'project_list'
		return context

class ProjectDetailView(DetailView):
	model=Project
	def get_template_names(self):
		return 'project_detail.html'

@login_required
def create(request):
	"""
	Creates a new project entry
	"""
	if (request.method == 'POST') :
		create_form = CreateForm(request.POST)
		if create_form.is_valid():
			# Get the parameters from the form
			in_title = request.POST['title']
			in_platform = request.POST['platform']
			in_description = request.POST['description']
			in_user = request.user
			# Add the new project to the database
			new_project = Project(
				title = in_title,
				user = in_user,
				platform = in_platform,
				description = in_description)

			new_project.save()
			
			# Redirect to the Project list page
			return HttpResponseRedirect(reverse('index'))
	else:
		create_form = CreateForm()

	return render(request, 'create.html',
		      {'create_form': create_form})
