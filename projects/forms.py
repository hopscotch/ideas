from django import forms
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit
from crispy_forms.bootstrap import FormActions

class CreateForm(forms.Form):
	"""
	Defines the form to create a new poject
	"""
	# Project Title Field
	title = forms.CharField(
		widget=forms.TextInput(attrs={'class':'input-large'}),
		label = 'Project Title', 
		max_length = 35,
		required = True
	)
	
	# Project Platform Field
	platform = forms.CharField(
		label = 'Platform',
		max_length = 35,
		required = False
	)	
	
	# Project Description
	description = forms.CharField(
		widget=forms.Textarea(attrs={'class':'input-xxlarge'}),
		label = 'Description',
		required = True,
	)
	
	# Constructor to add some attributes
	def __init__(self, *args, **kwargs):
		self.helper = FormHelper()
		
		# Attributes
		self.helper.id = 'createForm'
		self.helper.form_class = 'form-horizontal well'
		self.helper.form_method = 'post'
		
		# Submit Button
		self.helper.add_input(Submit('submit', 'Create'))
		
		super(CreateForm, self).__init__(*args, **kwargs)
		
