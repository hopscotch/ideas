from django.conf.urls import patterns, url
import views

urlpatterns = patterns('projects.views',
	# Default projects page shows lists of projects 
	url(r'^$', views.ProjectListView.as_view(), name='index'),
	# Shows project details (ex: 3/)
        url(r'^(?P<pk>\d+)/$', views.ProjectDetailView.as_view(), name='details'),
	# Shows the create project page (ex: create/)
	url(r'^create/$', 'create', name='create')
)
